#include "MPU6050.h"
#include "myI2C.h"
#include "Delay.h"
#include <math.h>

uint8_t MPU_Init_Flag = 0;

#define MPU6050_ADDRESS 0xD0
#define PI 3.14
int16_t AX, AY, AZ, GX, GY, GZ;
float Roll, Pitch, Yaw;

float Gyro_X, Gyro_Y, Gyro_Z;
float dt = 0.01;
float Accel_Angle_x, Accel_Angle_y;

float Err, Err_Average;
uint16_t Err_Counter;
uint8_t Correct_State;

void MPU6050_Init(void)
{
      uint8_t res;

      MPU6050_WriteReg(MPU6050_PWR_MGMT_1, 0x80);
      Delay_ms(100);
      MPU6050_WriteReg(MPU6050_PWR_MGMT_1, 0x00);

      MPU6050_WriteReg(MPU6050_GYRO_CONFIG, 0x18);
      MPU6050_WriteReg(MPU6050_ACCEL_CONFIG, 0x18);

      MPU6050_WriteReg(MPU6050_SMPLRT_DIV, 0x09);
      MPU6050_WriteReg(MPU6050_CONFIG, 0x06);

      MPU6050_WriteReg(MPU6050_RA_INT_PIN_CFG, 0x80);
      MPU6050_WriteReg(MPU6050_RA_INT_ENABLE, 0x01);

      res = MPU6050_GetID();
      if (res == 0x68)
      {
            MPU6050_WriteReg(MPU6050_PWR_MGMT_1, 0x02);
            MPU6050_WriteReg(MPU6050_PWR_MGMT_2, 0x00);
            Delay_ms(1000);
            MPU_Init_Flag = 1;
      }
}

float Complementary_Filter_x(float angle_m, float gyro_m)
{
      static float angle1;
      float K1 = 0.02;
      angle1 = K1 * angle_m + (1 - K1) * (angle1 + gyro_m * dt);
      return angle1;
}
float Complementary_Filter_y(float angle_m, float gyro_m)
{
      static float angle2;
      float K1 = 0.2;
      angle2 = K1 * angle_m + (1 - K1) * (angle2 + gyro_m * dt);
      return angle2;
}
void Gyro_Z_Correct()
{
      if (Correct_State == 0)
      {
            Err_Counter++;
            Err += Gyro_Z;
            if (Err_Counter >= 400)
            {
                  Err_Average = Err / Err_Counter;
                  Correct_State = 1;
            }
      }
      else if (Correct_State == 1)
      {
            Yaw += Gyro_Z;
            Yaw -= Err_Average;
      }
}
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
      if (GPIO_Pin == MPU_INT_Pin && MPU_Init_Flag == 1)
      {
            MPU6050_GetData(&AX, &AY, &AZ, &GX, &GY, &GZ);
            Accel_Angle_y = atan2(AX, AZ) * 180 / PI; // 计算倾角，转换单位为度
            Accel_Angle_x = atan2(AY, AZ) * 180 / PI; // 计算倾角，转换单位为度
            Gyro_X = GX;
            Gyro_Y = GY;
            Gyro_Z = GZ;
            Gyro_X = Gyro_X / 16.4; // 陀螺仪量程转换，量程±2000°/s对应灵敏度16.4，可查手册
            Gyro_Y = Gyro_Y / 16.4; // 陀螺仪量程转换
            Gyro_Z = Gyro_Z / 16.4;
            Roll = -Complementary_Filter_y(Accel_Angle_y, Gyro_Y);
            Pitch = -Complementary_Filter_x(Accel_Angle_x, Gyro_X); // 互补滤波
            Gyro_Z_Correct();
      }
}

uint8_t MPU6050_GetID(void)
{
      return MPU6050_ReadReg(MPU6050_WHO_AM_I);
}

void MPU6050_GetData(int16_t *AccX, int16_t *AccY, int16_t *AccZ,
                     int16_t *GyroX, int16_t *GyroY, int16_t *GyroZ)
{
      uint8_t DataH, DataL;

      DataH = MPU6050_ReadReg(MPU6050_ACCEL_XOUT_H);
      DataL = MPU6050_ReadReg(MPU6050_ACCEL_XOUT_L);
      *AccX = (DataH << 8) | DataL;

      DataH = MPU6050_ReadReg(MPU6050_ACCEL_YOUT_H);
      DataL = MPU6050_ReadReg(MPU6050_ACCEL_YOUT_L);
      *AccY = (DataH << 8) | DataL;

      DataH = MPU6050_ReadReg(MPU6050_ACCEL_ZOUT_H);
      DataL = MPU6050_ReadReg(MPU6050_ACCEL_ZOUT_L);
      *AccZ = (DataH << 8) | DataL;

      DataH = MPU6050_ReadReg(MPU6050_GYRO_XOUT_H);
      DataL = MPU6050_ReadReg(MPU6050_GYRO_XOUT_L);
      *GyroX = (DataH << 8) | DataL;

      DataH = MPU6050_ReadReg(MPU6050_GYRO_YOUT_H);
      DataL = MPU6050_ReadReg(MPU6050_GYRO_YOUT_L);
      *GyroY = (DataH << 8) | DataL;

      DataH = MPU6050_ReadReg(MPU6050_GYRO_ZOUT_H);
      DataL = MPU6050_ReadReg(MPU6050_GYRO_ZOUT_L);
      *GyroZ = (DataH << 8) | DataL;
}
void MPU6050_WriteReg(uint8_t RegAddress, uint8_t Data)
{
      MyI2C_Start();
      MyI2C_SendByte(MPU6050_ADDRESS);
      MyI2C_ReceiveAck();
      MyI2C_SendByte(RegAddress);
      MyI2C_ReceiveAck();
      MyI2C_SendByte(Data);
      MyI2C_ReceiveAck();
      MyI2C_Stop();
}

uint8_t MPU6050_ReadReg(uint8_t RegAddress)
{
      uint8_t Data;

      MyI2C_Start();
      MyI2C_SendByte(MPU6050_ADDRESS);
      MyI2C_ReceiveAck();
      MyI2C_SendByte(RegAddress);
      MyI2C_ReceiveAck();

      MyI2C_Start();
      MyI2C_SendByte(MPU6050_ADDRESS | 0x01);
      MyI2C_ReceiveAck();
      Data = MyI2C_ReceiveByte();
      MyI2C_SendAck(1);
      MyI2C_Stop();

      return Data;
}
