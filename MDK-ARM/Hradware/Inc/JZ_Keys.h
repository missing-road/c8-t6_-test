#ifndef __JZ_KEYS_H
#define __JZ_KEYS_H
#define R1_L HAL_GPIO_WritePin(R1_GPIO_Port,R1_Pin,GPIO_PIN_RESET)
#define R2_L HAL_GPIO_WritePin(R2_GPIO_Port,R2_Pin,GPIO_PIN_RESET)
#define R3_L HAL_GPIO_WritePin(R3_GPIO_Port,R3_Pin,GPIO_PIN_RESET)
#define R4_L HAL_GPIO_WritePin(R4_GPIO_Port,R4_Pin,GPIO_PIN_RESET)
#define R1_H HAL_GPIO_WritePin(R1_GPIO_Port,R1_Pin,GPIO_PIN_SET)
#define R2_H HAL_GPIO_WritePin(R2_GPIO_Port,R2_Pin,GPIO_PIN_SET)
#define R3_H HAL_GPIO_WritePin(R3_GPIO_Port,R3_Pin,GPIO_PIN_SET)
#define R4_H HAL_GPIO_WritePin(R4_GPIO_Port,R4_Pin,GPIO_PIN_SET)
#include "main.h"

void JZ_Keys_Scan(void);

#endif
