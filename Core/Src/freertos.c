/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

	  uint8_t TX_Buf[16];
	  uint8_t message[16] = "LED3Blink";
/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */
/* USER CODE END Variables */
/* Definitions for defaultTask */
osThreadId_t defaultTaskHandle;
const osThreadAttr_t defaultTask_attributes = {
  .name = "defaultTask",
  .stack_size = 128 * 4,
  .priority = (osPriority_t) osPriorityNormal,
};
/* Definitions for LED2BlinkTask */
osThreadId_t LED2BlinkTaskHandle;
const osThreadAttr_t LED2BlinkTask_attributes = {
  .name = "LED2BlinkTask",
  .stack_size = 128 * 4,
  .priority = (osPriority_t) osPriorityNormal,
};
/* Definitions for LED3BlinkTask */
osThreadId_t LED3BlinkTaskHandle;
const osThreadAttr_t LED3BlinkTask_attributes = {
  .name = "LED3BlinkTask",
  .stack_size = 128 * 4,
  .priority = (osPriority_t) osPriorityNormal,
};
/* Definitions for ComProcessTask */
osThreadId_t ComProcessTaskHandle;
const osThreadAttr_t ComProcessTask_attributes = {
  .name = "ComProcessTask",
  .stack_size = 128 * 4,
  .priority = (osPriority_t) osPriorityHigh,
};
/* Definitions for UartTXQueue */
osMessageQueueId_t UartTXQueueHandle;
const osMessageQueueAttr_t UartTXQueue_attributes = {
  .name = "UartTXQueue"
};
/* Definitions for BTtransMutex */
osMutexId_t BTtransMutexHandle;
const osMutexAttr_t BTtransMutex_attributes = {
  .name = "BTtransMutex"
};
/* Definitions for TaskNumMutex */
osMutexId_t TaskNumMutexHandle;
const osMutexAttr_t TaskNumMutex_attributes = {
  .name = "TaskNumMutex"
};

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */

/* USER CODE END FunctionPrototypes */

void StartDefaultTask(void *argument);
void LED2Blink(void *argument);
void LED3Blink(void *argument);
void ComProcess(void *argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/**
  * @brief  FreeRTOS initialization
  * @param  None
  * @retval None
  */
void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */

  /* USER CODE END Init */
  /* Create the mutex(es) */
  /* creation of BTtransMutex */
  BTtransMutexHandle = osMutexNew(&BTtransMutex_attributes);

  /* creation of TaskNumMutex */
  TaskNumMutexHandle = osMutexNew(&TaskNumMutex_attributes);

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* Create the queue(s) */
  /* creation of UartTXQueue */
  UartTXQueueHandle = osMessageQueueNew (16, sizeof(uint8_t), &UartTXQueue_attributes);

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* creation of defaultTask */
  defaultTaskHandle = osThreadNew(StartDefaultTask, NULL, &defaultTask_attributes);

  /* creation of LED2BlinkTask */
  LED2BlinkTaskHandle = osThreadNew(LED2Blink, NULL, &LED2BlinkTask_attributes);

  /* creation of LED3BlinkTask */
  LED3BlinkTaskHandle = osThreadNew(LED3Blink, NULL, &LED3BlinkTask_attributes);

  /* creation of ComProcessTask */
  ComProcessTaskHandle = osThreadNew(ComProcess, NULL, &ComProcessTask_attributes);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

  /* USER CODE BEGIN RTOS_EVENTS */
  /* add events, ... */
  /* USER CODE END RTOS_EVENTS */

}

/* USER CODE BEGIN Header_StartDefaultTask */
/**
  * @brief  Function implementing the defaultTask thread.
  * @param  argument: Not used
  * @retval None
  */
/* USER CODE END Header_StartDefaultTask */
void StartDefaultTask(void *argument)
{
  /* USER CODE BEGIN StartDefaultTask */
  /* Infinite loop */
  for(;;)
  {
	  OLED_Clearrow(0);
	  osMutexAcquire(TaskNumMutexHandle, osWaitForever);
	  char* message = "StartDefaultTask";
//	  osMessageQueuePut(UartTXQueueHandle,message,0,10);
	  Task_Num = "DefaultTask";
	  OLED_ShowString(0,0,(uint8_t*)Task_Num,16);//OLED显示
	  osMutexRelease(TaskNumMutexHandle);
	  
	  
	  RGB_RED(1);
	  delay_ms(1000);
	  RGB_GREEN(1);
	  delay_ms(1000);
	  RGB_BLUE(1);
	  delay_ms(1000);
	  
  }
  /* USER CODE END StartDefaultTask */
}

/* USER CODE BEGIN Header_LED2Blink */
/**
* @brief Function implementing the LED2BlinkTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_LED2Blink */
void LED2Blink(void *argument)
{
  /* USER CODE BEGIN LED2Blink */
  /* Infinite loop */
  for(;;)
  {
	  OLED_Clearrow(0);
	  osMutexAcquire(TaskNumMutexHandle, osWaitForever);
	  char* message = "LED2Blink";
//	  osMessageQueuePut(UartTXQueueHandle,message,0,10);
	  Task_Num = "LED2Blink";
	  OLED_ShowString(0,0,(uint8_t*)Task_Num,16);//OLED显示
	  osMutexRelease(TaskNumMutexHandle);
	  LED2_ON;
	  osDelay(500);
	  LED2_OFF;
	  osDelay(500);
  }
  /* USER CODE END LED2Blink */
}

/* USER CODE BEGIN Header_LED3Blink */
/**
* @brief Function implementing the LED3BlinkTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_LED3Blink */
void LED3Blink(void *argument)
{
  /* USER CODE BEGIN LED3Blink */
  /* Infinite loop */
  for(;;)
  {
	  OLED_Clearrow(0);
	  osMutexAcquire(TaskNumMutexHandle, osWaitForever);
	  uint8_t myCharArray[16];  
  
  /* 在这里填充你的char数组。 */  
  myCharArray[0] = 'H';  
  myCharArray[1] = 'e';  
  myCharArray[2] = 'l';  
  myCharArray[3] = 'l';  
  myCharArray[4] = 'o';  
  myCharArray[5] = ',';  
  myCharArray[6] = ' ';  
  myCharArray[7] = 'W';  
  myCharArray[8] = 'o';  
  myCharArray[9] = 'r';  
  myCharArray[10] = 'l';  
  myCharArray[11] = 'd';  
  myCharArray[12] = '!';  
  myCharArray[13] = '1'; // 一定要在最后加上字符串结束符。  
  myCharArray[14] = '2';  
  myCharArray[15] = '\0';  
  uint8_t* arr = myCharArray;
	  osMessageQueuePut(UartTXQueueHandle,arr,0,0);
//	  for(int i =0;i<sizeof(message);i++)
//	  {
//		osMessageQueuePut(UartTXQueueHandle,(void*)(message+i),0,osWaitForever);
//	  }
	  Task_Num = "LED3Blink";
	  OLED_ShowString(0,0,(uint8_t*)Task_Num,16);//OLED显示
	  osMutexRelease(TaskNumMutexHandle);
	  LED3_ON;
	  osDelay(500);
	  LED3_OFF;
	  osDelay(500);
  }
  /* USER CODE END LED3Blink */
}

/* USER CODE BEGIN Header_ComProcess */
/**
* @brief Function implementing the ComProcessTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_ComProcess */
void ComProcess(void *argument)
{
  /* USER CODE BEGIN ComProcess */
  /* Infinite loop */
  for(;;)
  {
//	  for(int i = 0;i<16;i++)
//	  {
//		  TX_Buf[i] = 0xff;
//	  }
	  if(osMessageQueueGet(UartTXQueueHandle,TX_Buf,NULL,osWaitForever)==osOK)
	  {
		  uint8_t* arr = TX_Buf;
		  HAL_UART_Transmit(&huart2,(const uint8_t*)TX_Buf,sizeof(TX_Buf),HAL_MAX_DELAY);
			OLED_ShowString(2,2,arr,16);
//		  printf("%s",TX_Buf);
	  }
  }
  /* USER CODE END ComProcess */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */

/* USER CODE END Application */

